SpreeApiJwt
========

Installation
------------

Add spree_api_jwt to your Gemfile:

```ruby
gem 'spree_api_jwt'
```

Bundle your dependencies and run the installation generator:

```shell
bundle
bundle exec rails g spree_api_jwt:install
```
