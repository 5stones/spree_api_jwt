module Spree
  class SpreeApiJwtConfiguration < Preferences::Configuration
    preference :algorithm, :string, :default => "none"
    preference :encode_key, :string, :default => nil
    preference :decode_key, :string, :default => nil
    preference :hours_to_expire, :integer, :default => 24
  end
end
