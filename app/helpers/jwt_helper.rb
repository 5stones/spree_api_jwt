require 'jwt'

class JwtHelper
  def self.get_token(user)
    payload = {
      :user_id => user.id,
      :exp => Spree::ApiJwtConfiguration[:hours_to_expire].hours.from_now.to_i
    }
    token = JWT.encode payload, Spree::ApiJwtConfiguration[:encode_key], Spree::ApiJwtConfiguration[:algorithm]
    token
  end
end
