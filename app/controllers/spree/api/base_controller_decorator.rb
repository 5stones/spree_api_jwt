require 'jwt'

Spree::Api::BaseController.class_eval do
  def jwt_payload
    header = request.headers["Authorization"]

    if header
      header.slice! "Bearer "
    end

    header || params[:json_web_token]
  end
  helper_method :jwt_payload

  def load_user
    @current_api_user = Spree.user_class.find_by(spree_api_key: api_key.to_s)
    load_jwt_user if !@current_api_user
  end

  def load_jwt_user
    begin
      decoded_token = JWT.decode(jwt_payload, nil, false)
    rescue JWT::DecodeError
      render "spree/api/errors/must_include_authentication", status: 401 and return
    end
    user_id = decoded_token.first.fetch('user_id').to_i
    @current_api_user = Spree.user_class.find user_id
  end
end
