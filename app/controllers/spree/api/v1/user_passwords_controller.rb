module Spree
  module Api
    module V1
      class UserPasswordsController < Devise::PasswordsController
        skip_before_action :verify_authenticity_token, raise: false

        def update
          self.resource = resource_class.reset_password_by_token(params)

          if resource.errors.empty?
            token = JwtHelper.get_token(resource)

            expires_in 15.minutes, public: true
            (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))

            headers['Surrogate-Control'] = "max-age=#{15.minutes}"
            headers['Surrogate-Key'] = "group_id=1"
            respond_to do |format|
              format.json { render json: {'token' => token }, status: :ok }
            end
          else
            respond_to do |format|
              format.json { render json: resource.errors.as_json, status: 422 }
            end
          end
        end
      end
    end
  end
end
