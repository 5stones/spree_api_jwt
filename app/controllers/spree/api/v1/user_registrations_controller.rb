module Spree
  module Api
    module V1
      class UserRegistrationsController < Spree::Api::BaseController
        skip_before_action :verify_authenticity_token, raise: false

        def update
          if current_api_user.update_with_password(user_params)
            respond_to do |format|
              format.json { render nothing: true, status: :ok }
            end
          else
            respond_to do |format|
              format.json { render json: { errors: current_api_user.errors.full_messages }, status: :unprocessable_entity }
            end
          end
        end

        private

        def user_params
          params.require(:user).permit(:current_password, :password, :password_confirmation)
        end

      end
    end
  end
end
