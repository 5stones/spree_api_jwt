module Spree
  module Api
    module V1
      class UserSessionsController < Devise::SessionsController
        skip_before_action :verify_authenticity_token, raise: false

        def create
          authenticate_spree_user!
          if spree_user_signed_in?
            token = JwtHelper.get_token(current_spree_user)
            # payload = {
            #   :user_id => current_spree_user.id,
            #   :exp => Spree::ApiJwtConfiguration[:hours_to_expire].hours.from_now.to_i
            # }
            # token = JWT.encode payload, Spree::ApiJwtConfiguration[:encode_key], Spree::ApiJwtConfiguration[:algorithm]
            expires_in 15.minutes, public: true

            # Devise will keep you signed in until the next sign out so if the same session tries to sign in as someone else it will always succeed
            (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))

            headers['Surrogate-Control'] = "max-age=#{15.minutes}"
            headers['Surrogate-Key'] = "group_id=1"
            respond_to do |format|
              format.json { render json: {'token' => token}, status: :created }
            end
          else
            respond_to do |format|
              @resource = OpenStruct.new({ errors: { username: ['Invalid username or password.'] } })
              format.json { render "spree/api/errors/invalid_resource", status: 422 }
            end
          end
        end
      end
    end
  end
end
