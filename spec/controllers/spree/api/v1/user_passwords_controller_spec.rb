require 'spec_helper'
require 'spree/testing_support/bar_ability'

module Spree
  describe Api::V1::UserPasswordsController, type: :controller do
    render_views

    let(:token) { "TOKEN" }
    let(:user) { create(:user, reset_password_token: token) }
    let(:subject) { api_post :update, reset_password_token: token }

    before { @request.env['devise.mapping'] = Devise.mappings[:spree_user] }

    shared_examples_for "a successful login" do
      it "returns success 200" do
        expect(response.status).to eq(200)
      end

      it "the response should include a jwt" do
        expect(json_response["token"])
      end
    end

    describe "#update" do
      before do
        subject
      end

      context "for a valid password token" do
        it_behaves_like "a successful login"

        context "when a password and password_confirmation are included in the request" do
          let(:password) { "new_password" }
          let(:subject) { api_post :update, reset_password_token: token, password: password, password_confirmation: password }

          it_behaves_like "a successful login"

          it "changes the user's password" do
            expect(user.reload.password).to eq(password)
          end
        end
      end

      context "for an invalid password token" do
        let(:token) { "INVALID_TOKEN" }

        it "returns not found 404" do
          expect(response.status).to eq(404)
        end

        it "the response should include a message" do
          expect(json_response["message"]).to eq("invalid token")
        end

        it "the response should not include a jwt" do
          expect(json_response["token"]).to eq(nil)
        end
      end

    end
  end
end
