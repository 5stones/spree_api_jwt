require 'spec_helper'
require 'spree/testing_support/bar_ability'

module Spree
  describe Api::V1::UserRegistrationsController, type: :controller do
    render_views

    let(:user) { create(:user, password: "password") }

    before { @request.env['devise.mapping'] = Devise.mappings[:spree_user] }

    describe "#update" do
      let(:subject) { api_post :update, params }

      context "when current password is not included" do
        let(:params) { { user: { password: "newpass", password_confirmation: "newpass" } } }

        it "is not successful" do
          subject
          expect(response.status).to eq(422)
        end

        it "changes the user's password" do
          expect{ subject }.to change{ user.password }.to "newpass"
        end
      end

      context "for a valid new password and password_confirmation" do
        let(:params) { { user: { current_password: "password", password: "newpass", password_confirmation: "newpass" } } }

        it "is successful" do
          subject
          expect(response.status).to eq(200)
        end

        it "changes the user's password" do
          expect{ subject }.to change{ user.password }.to "newpass"
        end
      end

      context "for a mismatched password_confirmation" do
        let(:params) { { user: { current_password: "password", password: "newpass", password_confirmation: "DIFFERENT" } } }

        it "is not successful" do
          subject
          expect(response.status).to eq(422)
        end

        it "does not change the user's password" do
          expect{ subject }.to_not change{ user.password }
        end

        it "includes the errors in the response" do
          expect(json_response["errors"]).to include("Password Confirmation doesn't match Password")
        end
      end

    end
  end
end
