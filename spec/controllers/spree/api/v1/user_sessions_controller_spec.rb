require 'spec_helper'
require 'spree/testing_support/bar_ability'

module Spree
  describe Api::V1::UserSessionsController, type: :controller do
    render_views

    let(:user) { create(:user) }

    before { @request.env['devise.mapping'] = Devise.mappings[:spree_user] }

    context "#create" do
      context "when the username and password are correct" do
        before do
          api_post :create, spree_user: {email: user.email, password: user.password}
        end

        it "it should give a successful response" do
          expect(response.status).to eq(201)
        end

        it "the response should include a jwt" do
          expect(json_response["token"])
        end
      end
      context "when the username and password are not correct" do
        before do
          api_post :create, spree_user: {email: user.email, password: "WRONG PASSWORD"}
        end

        it "it should give a failed response" do
          expect(response.status).to eq(500)
          expect(json_response["message"]).to eq("unsuccessful login")
        end

        it "the response should not include a jwt" do
          expect(json_response["token"]).to eq(nil)
        end
      end
    end
  end
end
