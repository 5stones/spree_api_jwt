require 'spec_helper'

class FakesController < Spree::Api::BaseController
end

module Spree
  describe Api::BaseController, :type => :controller do
    let(:user) { create(:user) }

    render_views
    controller(Spree::Api::BaseController) do
      def index
        render :text => { "products" => [] }.to_json
      end
    end

    before do
      @routes = ActionDispatch::Routing::RouteSet.new.tap do |r|
        r.draw { get 'index', to: 'spree/api/base#index' }
      end
    end

    context "when there is bearer authorization in the header" do
      before do
        request.headers["Authorization"] = "Bearer HASH123"
      end

      it "should parse out the 'Bearer '" do
        assert_equal "HASH123", subject.jwt_payload
      end
    end

    context "when there is plain authorization in the header" do
      before do
        request.headers["Authorization"] = "HASH123"
      end

      it "should return the authorization" do
        assert_equal "HASH123", subject.jwt_payload
      end
    end

    context "when a super admin accesses the API (with spree api token)" do
      before do
        user.generate_spree_api_key!
        request.headers["X-Spree-Token"] = user.spree_api_key
      end
      it "should allow access" do
        api_get :index
        expect(response.status).to eq(200)
      end
    end

    context "when a normal user accesses the api" do
      context "when there is no authentication" do
        it "should alert the user" do
          api_get :index
          expect(response.status).to eq(401)
          expect(json_response["error"]).to eq("You must include valid authentication.")
        end
      end

      context "when the jwt is invalid" do
        it "should alert the user" do
          api_get :index, json_web_token: "ABC123"
          expect(response.status).to eq(401)
          expect(json_response["error"]).to eq("You must include valid authentication.")
        end
      end

      context "when the jwt points to an existing user" do
        it "should allow api access" do
          json_web_token = JwtHelper.get_token(user)
          api_get :index, json_web_token: json_web_token
          expect(response.status).to eq(200)
        end
      end
    end
  end
end
