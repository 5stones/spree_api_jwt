Spree::Core::Engine.routes.draw do
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      devise_scope :spree_user do
        get '/authorization_failure', :to => 'user_sessions#authorization_failure', :as => :unauthorized
        post '/login' => 'user_sessions#create', :as => :create_new_session
        get '/logout' => 'user_sessions#destroy', :as => :logout

        patch '/users' => 'user_registrations#update', :as => :update_registration
        put '/password/change' => 'user_passwords#update', :as => :update_password
      end
    end
  end
end
